from fastapi import FastAPI
from server.routers.users import users_router
from server.routers.tournament import tournmanet_router
from server.routers.match import match_router
from server.routers.players import players_router


app = FastAPI()
app.include_router(users_router)
app.include_router(tournmanet_router)
app.include_router(match_router)
app.include_router(players_router)
