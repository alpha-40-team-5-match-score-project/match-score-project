import unittest
from unittest.mock import Mock, patch
from server.data.models import Tournaments, Tournament_registration
from server.routers import tournament as tournament_router
from server.common.response import NotFound, Unauthorized

mock_tournament_services = Mock(spec="server.services.tournament_services")
tournament_router.tournament_services = mock_tournament_services


def fake_user_director_auth() -> Mock:
    fake_user = Mock()
    fake_user.is_director = lambda: True
    tournament_router.get_user_or_raise_401 = Mock()
    tournament_router.get_user_or_raise_401.return_value = fake_user

    return fake_user


def fake_user_auth() -> Mock:
    fake_user = Mock()
    fake_user.is_director = lambda: False
    tournament_router.get_user_or_raise_401 = Mock()
    tournament_router.get_user_or_raise_401.return_value = fake_user

    return fake_user


def fake_tournament_registration0(
    id=1, tournament_id=1, player_name="Ivan", player_total_score=0
):
    fake_tournament_registration = Mock(spec=Tournament_registration)
    fake_tournament_registration.id = id
    fake_tournament_registration.tournament_id = tournament_id
    fake_tournament_registration.player_name = player_name
    fake_tournament_registration.player_total_score = player_total_score

    return fake_tournament_registration


def fake_tournament_registration1(
    id=2, tournament_id=1, player_name="Stoyan", player_total_score=0
):
    fake_tournament_registration = Mock(spec=Tournament_registration)
    fake_tournament_registration.id = id
    fake_tournament_registration.tournament_id = tournament_id
    fake_tournament_registration.player_name = player_name
    fake_tournament_registration.player_total_score = player_total_score

    return fake_tournament_registration


def fake_tournaments(
    id=1,
    title="UEFA",
    location="Qatar",
    start_date="2022-11-20",
    end_date="2022-12-20",
    tournament_types_id=1,
    prize=1,
):
    fake_tournament = Mock(spec=Tournaments)
    fake_tournament.id = id
    fake_tournament.title = title
    fake_tournament.location = location
    fake_tournament.start_date = start_date
    fake_tournament.end_date = end_date
    fake_tournament.tournament_types_id = tournament_types_id
    fake_tournament.prize = prize

    return fake_tournament


class TournamentRouterShould(unittest.TestCase):
    def setUp(self) -> None:
        mock_tournament_services.reset_mock(return_value=True, side_effect=True)

    def test_viewAllTournament_works_correctly(self):
        tournament0 = fake_tournaments()

        mock_tournament_services.all = lambda: [tournament0]

        result = tournament_router.view_all_tournaments()
        expected = 1

        self.assertEqual(expected, len(result))

    def test_viewAllTournament_return_NotFound_when_no_data_present(self):
        mock_tournament_services.all = lambda: []

        result = tournament_router.view_all_tournaments()
        expected = NotFound

        self.assertEqual(expected, type(result))

    def test_getById_works_correctly_when_data_present(self):
        tournamnet0 = fake_tournaments()
        mock_tournament_services.get_by_id = lambda t: [tournamnet0]

        result = tournament_router.get_by_id(1)
        expected = 1

        self.assertEqual(expected, len(result))

    def test_getById_return_NotFound_when_no_data_present(self):
        mock_tournament_services.get_by_id = lambda t: []

        result = tournament_router.get_by_id(1)
        expected = NotFound

        self.assertEqual(expected, type(result))

    def test_createTournament_works_correctly(self):
        tournament0 = fake_tournaments()
        mock_tournament_services.create_tournament = lambda q: [tournament0]

        result = tournament_router.create(
            tournament0, x_token=fake_user_director_auth()
        )
        expected = 1

        self.assertEqual(expected, len(result))

    def test_createTournament_return_Unauthorized_correctly(self):
        tournament0 = fake_tournaments()
        mock_tournament_services.create_tournament = lambda q: [tournament0]

        result = tournament_router.create(tournament0, x_token=fake_user_auth())
        expected = Unauthorized

        self.assertEqual(expected, type(result))

    def test_insertPlayersInTournament_works_correctly(self):
        register0 = fake_tournament_registration0()
        mock_tournament_services.check_player_name = lambda n, id: None
        mock_tournament_services.insert_players_in_tournament = lambda q: [register0]

        result = tournament_router.insert_players(
            players=register0, x_token=fake_user_director_auth()
        )
        expected = 1

        self.assertEqual(expected, len(result))

    def test_insertPlayersInTournament_return_Unauthorized_when_playerAlreadyInserted(
        self,
    ):
        register0 = fake_tournament_registration0()
        mock_tournament_services.check_player_name = lambda n, id: "Ivan"
        mock_tournament_services.insert_players_in_tournament = lambda q: register0

        result = tournament_router.insert_players(
            players=register0, x_token=fake_user_auth()
        )
        expected = Unauthorized

        self.assertEqual(expected, type(result))
