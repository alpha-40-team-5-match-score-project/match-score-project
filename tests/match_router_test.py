import unittest
from unittest.mock import Mock, patch
from server.data.models import Matches, Tournament_registration
from server.routers import match as match_router
from server.common.response import NotFound, BadRequest

mock_match_services = Mock(spec="server.services.match_services")
match_router.match_services = mock_match_services


def fake_match(
    match_id=1,
    round=1,
    date="2022-11-30",
    tournament_id=1,
    score_one=0,
    score_two=0,
    player_one="Ivan",
    player_two="Stoyan",
):
    mock_match = Mock(spec=Matches)
    mock_match.match_id = match_id
    mock_match.round = round
    mock_match.date = date
    mock_match.tournament_id = tournament_id
    mock_match.score_one = score_one
    mock_match.score_two = score_two
    mock_match.player_one = player_one
    mock_match.player_two = player_two

    return mock_match


def fake_match_winner_one(
    match_id=1,
    round=1,
    date="2022-11-30",
    tournament_id=1,
    score_one=2,
    score_two=0,
    player_one="Ivan",
    player_two="Stoyan",
    winner="Ivan",
):
    mock_match = Mock(spec=Matches)
    mock_match.match_id = match_id
    mock_match.round = round
    mock_match.date = date
    mock_match.tournament_id = tournament_id
    mock_match.score_one = score_one
    mock_match.score_two = score_two
    mock_match.player_one = player_one
    mock_match.player_two = player_two
    mock_match.winner = winner

    return mock_match


def fake_match_winner_two(
    match_id=1,
    round=1,
    date="2022-11-30",
    tournament_id=1,
    score_one=0,
    score_two=2,
    player_one="Ivan",
    player_two="Stoyan",
):
    mock_match = Mock(spec=Matches)
    mock_match.match_id = match_id
    mock_match.round = round
    mock_match.date = date
    mock_match.tournament_id = tournament_id
    mock_match.score_one = score_one
    mock_match.score_two = score_two
    mock_match.player_one = player_one
    mock_match.player_two = player_two

    return mock_match


def fake_tournament_standing(
    id=1, tournament_id=1, player_name="Ivan", player_total_score=4
):
    mock_stadings = Mock(spec=Tournament_registration)
    mock_stadings.id = id
    mock_stadings.tournament_id = tournament_id
    mock_stadings.player_name = player_name
    mock_stadings.player_total_score = player_total_score

    return mock_stadings


def fake_user_director_auth() -> Mock:
    fake_user = Mock()
    fake_user.is_admin = lambda: True
    match_router.get_user_or_raise_401 = Mock()
    match_router.get_user_or_raise_401.return_value = fake_user

    return fake_user


class PlayerRouterShould(unittest.TestCase):
    def setUp(self) -> None:
        mock_match_services.reset_mock(return_value=True, side_effect=True)

    def test_findMAtchById_works_correctly(self):
        match0 = fake_match()

        mock_match_services.find_match_by_id = lambda match: [match0]
        result = match_router.get_match_by_id(1)

        self.assertEqual(1, len(result))

    def test_findMatchById_return_Unauthorized_when_no_data(self):
        mock_match_services.find_match_by_id = lambda match: []
        result = match_router.get_match_by_id(1)

        self.assertEqual(NotFound, type(result))

    def test_createMatch_works_correctly(self):
        match0 = fake_match()
        mock_match_services.check_tournament_type = Mock()
        mock_match_services.find_player_email_to_send = lambda m: ("Ivan", "Stoyan")
        mock_match_services.create_league_match = lambda m: match0

        result = match_router.create_matches(match0, x_token=fake_user_director_auth())

        self.assertEqual(match0, result)

    def test_createMatch_return_badRequest_when_not_login(self):
        match0 = fake_match()
        mock_match_services.check_tournament_type = Mock()
        mock_match_services.find_player_email_to_send = lambda m: ("Ivan", "Stoyan")
        mock_match_services.create_league_match = lambda m: match0

        result = match_router.create_matches(match0, x_token=None)

        self.assertEqual(BadRequest, type(result))

    def test_startMatch_return_tieResult_correctly(self):
        match0 = fake_match()
        mock_match_services.find_tournament_by_id = lambda q, id: match0
        mock_match_services.assign_winner = Mock()
        mock_match_services.check_to_proceed_next_round = Mock()
        mock_match_services.start_matches = lambda m, id: match0

        result = match_router.start_match(match0, 1, x_token=fake_user_director_auth())
        expected = "This round ended in a tie!"
        self.assertEqual(expected, result)

    def test_startMatch_return_playerOne_asWinner_correctly(self):
        match0 = fake_match_winner_one()
        mock_match_services.find_tournament_by_id = lambda q, id: match0
        mock_match_services.assign_winner = Mock()
        mock_match_services.check_to_proceed_next_round = Mock()
        mock_match_services.start_matches = lambda m, id: match0

        result = match_router.start_match(match0, 1, x_token=fake_user_director_auth())
        expected = "The winner in round 1 is Ivan"
        self.assertEqual(expected, result)

    def test_startMatch_return_playerTwo_asWinner_correctly(self):
        match0 = fake_match_winner_two()
        mock_match_services.find_tournament_by_id = lambda q, id: match0
        mock_match_services.assign_winner = Mock()
        mock_match_services.check_to_proceed_next_round = Mock()
        mock_match_services.start_matches = lambda m, id: match0

        result = match_router.start_match(match0, 1, x_token=fake_user_director_auth())
        expected = "The winner in round 1 is Stoyan"
        self.assertEqual(expected, result)

    def test_startMatch_return_notFound_when_no_match_found(self):
        match0 = fake_match_winner_two()
        mock_match_services.find_tournament_by_id = Mock()
        mock_match_services.assign_winner = Mock()
        mock_match_services.check_to_proceed_next_round = Mock()
        mock_match_services.start_matches = lambda m, id: match0

        result = match_router.start_match(match0, 1, x_token=fake_user_director_auth())
        expected = NotFound
        self.assertEqual(expected, type(result))

    def test_checkResult_works_correctly(self):
        match0 = fake_match_winner_one()
        mock_match_services.check_tournament_type = lambda q: "league"
        mock_match_services.check_is_league_finished_or_not = lambda q: 1
        mock_match_services.tournament_standings = lambda q: 1

        result = match_router.check_results(match0)
        expected = (
            "The league tournament is still is process! You can seee available score below."
        ), 1

        self.assertEqual(expected, result)
