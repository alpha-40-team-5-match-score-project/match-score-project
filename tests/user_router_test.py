import unittest
from unittest.mock import Mock
from server.data.models import LoginData
from server.routers import users as user_router
from server.common.response import Unauthorized

mock_user_services = Mock(spec="server.services.user_services")
user_router.user_services = mock_user_services


def fake_user_director_auth() -> Mock:
    fake_user = Mock()
    fake_user.is_director = lambda: True
    user_router.get_user_or_raise_401 = Mock()
    user_router.get_user_or_raise_401.return_value = fake_user

    return fake_user


def fake_user_auth() -> Mock:
    fake_user = Mock()
    fake_user.is_admin = lambda: False
    user_router.get_user_or_raise_401 = Mock()
    user_router.get_user_or_raise_401.return_value = fake_user

    return fake_user


def fake_login_data(email="testemail@abv.bg", password="123321A"):
    fake_login = Mock(spec=LoginData)
    fake_login.email = email
    fake_login.password = password

    return fake_login


class UserRouterShould(unittest.TestCase):
    def setUp(self) -> None:
        mock_user_services.reset_mock(return_value=True, side_effect=True)

    def test_userLogin_works_correctly(self):
        login1 = fake_login_data()
        mock_user_services.try_login = lambda l, t: login1

        result = user_router.login(fake_login_data())
        expected = True

        self.assertEqual(expected, bool(result))

    def test_register_works_correctly(self):
        login1 = fake_login_data()
        mock_user_services.create = lambda l, t: login1

        result = user_router.register(login1)
        expected = True

        self.assertEqual(True, bool(result))

    def test_reqeustDirector_works_correctly(self):
        mock_user_services.request_promotion_to_director = Mock()

        result = user_router.request_director_promotion(x_token=fake_user_auth())
        expected = "Your application has been forwarded! Pending acception/refusal."

        self.assertEqual(expected, result)

    def test_requestPlayer_promotion_to_director_works_correctly(self):
        mock_user_services.find_by_id = Mock()
        mock_user_services.promote_to_director = Mock()

        result = user_router.promote_to_director(1, x_token=fake_user_director_auth())
        expected = "User has been promoted to Director!"

        self.assertEqual(expected, result)

    def test_requestPlayer_promotion_to_playerworks_correctly(self):
        mock_user_services.find_by_id = Mock()
        mock_user_services.promote_to_player = Mock()

        result = user_router.promote_to_player(1, x_token=fake_user_director_auth())
        expected = "User has been promoted to Player!"

        self.assertEqual(expected, result)

    def test_refusePlayer_promotion_works_correctly(self):
        mock_user_services.find_by_id = Mock()
        mock_user_services.refuse_user_by_id = Mock()

        result = user_router.refuse_user(1, x_token=fake_user_director_auth())
        expected = "User application has been refused!"

        self.assertEqual(expected, result)

    def test_refusePlayer_promotion_works_correctly_when_noAdminOrDirector_try_approve(
        self,
    ):
        mock_user_services.find_by_id = Mock()
        mock_user_services.refuse_user_by_id = Mock()

        result = user_router.refuse_user(1, x_token=fake_user_auth())
        expected = Unauthorized

        self.assertEqual(expected, type(result))
