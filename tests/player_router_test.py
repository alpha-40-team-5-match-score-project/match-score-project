import unittest
from fastapi import HTTPException
from unittest.mock import Mock, patch
from server.data.models import Players, SportClubs
from server.routers import players as players_router
from server.routers import users as user_router
from server.common.response import NotFound

mock_player_services = Mock(spec="server.services.player_services")
mock_user_services = Mock(spec="server.common.auth")
players_router.player_services = mock_player_services
user_router.auth = mock_user_services


def fake_player(name="Ivan", country_code="BG", sport_club_id=1):
    mock_player = Mock(spec=Players)
    mock_player.name = name
    mock_player.country_code = country_code
    mock_player.sport_club_id = sport_club_id

    return mock_player


def fake_sport_club(id=1, name="UEFA", creation_date="2022-11-30"):
    mock_sport_club = Mock(spec=SportClubs)
    mock_sport_club.id = id
    mock_sport_club.name = name
    mock_sport_club.creation_date = creation_date

    return mock_sport_club


def fake_user_director_auth() -> Mock:
    fake_user = Mock()
    fake_user.is_director = lambda: True
    players_router.get_user_or_raise_401 = Mock()
    players_router.get_user_or_raise_401.return_value = fake_user

    return fake_user


def fake_player_user() -> Mock:
    fake_user = Mock()
    players_router.get_user_or_raise_401 = Mock()
    players_router.get_user_or_raise_401.return_value = fake_user

    return fake_user


class PlayerRouterShould(unittest.TestCase):
    def setUp(self) -> None:
        mock_player_services.reset_mock(return_value=True, side_effect=True)

    def tearDown(self) -> None:
        pass
        # reseting Mock a otgore se setupva

    def test_getAllPlayerInformation_works_correctly(self):
        player0 = fake_player(name="Ivan", country_code="BG", sport_club_id=1)
        player1 = fake_player(name="Stoyan", country_code="BG", sport_club_id=1)
        mock_player_services.show_all_players = lambda: [player0, player1]

        result = players_router.get_all_players()

        self.assertEqual(2, len(result))

    def test_getAllPlayer_return_NotFound_when_empty(self):
        mock_player_services.show_all_players = lambda: []

        result = type(players_router.get_all_players())
        expected = NotFound

        self.assertEqual(expected, result)

    def test_getPlayerByName_return_data_correctly(self):
        player0 = fake_player(name="Ivan", country_code="BG", sport_club_id=1)

        mock_player_services.get_player_information = lambda name: [player0]

        result = players_router.get_player_by_name("Ivan")

        self.assertEqual(1, len(result))

    def test_getPlayerByName_return_NotFound_when_no_playerFound(self):
        mock_player_services.get_player_information = lambda name: []

        result = type(players_router.get_player_by_name("Ivan"))

        self.assertEqual(NotFound, result)

    def test_createSportClub_works_correctly(self):
        mock_player_services.create_sport_club = Mock()

        result = players_router.create_sport_club(
            fake_sport_club(), x_token=fake_user_director_auth()
        )
        expected = "Sport Club created!"
        self.assertEqual(expected, result)

        # mock_player_services.create_sport_club.assert_called_once()
        # assert_called_once()
        # assert_not_called()

    def test_getUserOrRaise401_when_not_validToken(self):
        players_router.get_user_or_raise_401 = Mock()
        players_router.get_user_or_raise_401.side_effect = HTTPException(401)

        with self.assertRaises(HTTPException):
            players_router.create_sport_club(fake_sport_club(), x_token="")

    def test_playerUpdateProfile_works_correctly(self):
        mock_player_services.get_player_information = fake_player()
        mock_player_services.update_player_info = fake_player()

        result = players_router.update_profile(
            player=fake_player(), x_token=fake_user_director_auth()
        )
        expected = "Director has updated player info."

        self.assertEqual(expected, result)

    def test_playerUpdateProfile_returnsUnauthorized_when_no_directorOrAdmin(self):
        mock_player_services.get_player_information = fake_player()
        mock_player_services.update_player_info = fake_player()

        result = players_router.update_profile(
            player=fake_player(), x_token=fake_player_user()
        )
        expected = "Director has updated player info."

        self.assertEqual(expected, result)
