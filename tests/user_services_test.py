import unittest
from server.data.models import Users
from server.services import user_services


class UsersService_Should(unittest.TestCase):
    def test_find_by_email_works_correctly(self):
        get_data_func = lambda q, u: [(1, "test@gmail.com", "123", "guest", "")]
        result = user_services.find_by_email(
            "test@gmail.com", get_data_func=get_data_func
        )
        expected = Users(
            user_id=1,
            email="test@gmail.com",
            password="123",
            user_role="guest",
            player_name="",
        )

        self.assertEqual(expected, result)

    def test_find_by_id_works_correctly(self):
        get_data_func = lambda q, u: [(1, "test@gmail.com", "123", "guest", "")]
        result = user_services.find_by_id(1, get_data_func=get_data_func)
        expected = Users(
            user_id=1,
            email="test@gmail.com",
            password="123",
            user_role="guest",
            player_name="",
        )

        self.assertEqual(expected, result)

    def test_find_by_player_name_works_correctly(self):
        get_data_func = lambda q, u: "Test Player"
        result = user_services.find_user_by_player_name(
            "Test Player", get_data_func=get_data_func
        )
        expected = "Test Player"

        self.assertEqual(expected, result)

    def test_createUser_works_correctly(self):
        generated_id = 1
        insert_data_func = lambda q, u: generated_id

        result = user_services.create("test@gmail.com", "123", insert_data_func)
        expected = Users(
            user_id=1, email="test@gmail.com", password="", user_role="guest"
        )

        self.assertEqual(expected, result)

    def test_if_requestPromoteToPlayer_works_correctly(self):
        update_data_func = lambda update_user_role, n: update_user_role
        old = Users(
            user_id=1,
            email="test@gmail.com",
            password="123",
            user_role="guest",
            player_name="",
        )
        new = Users(
            user_id=1,
            email="test@gmail.com",
            password="123",
            user_role="pending approval",
            player_name="Test Player",
        )

        result = user_services.request_promotion_to_player(
            new, old, update_data_func=update_data_func
        )
        expected = Users(
            user_id=1,
            email="test@gmail.com",
            password="123",
            user_role="pending approval",
            player_name="Test Player",
        )

        self.assertEqual(expected, result)

    # def test_if_requestPromoteToDirector_works_correctly(self):
    #     update_data_func = lambda q,u: [(1,'test@gmail.com', '123', 'guest', '')]

    #     result = user_services.request_promotion_to_director(1, update_data_func)
    #     expected = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='pending approval')

    #     self.assertEqual(expected,result)

    # def test_if_promoteToDirector_works_correctly(self):
    #     update_data_func = lambda update_user_role: update_user_role
    #     old = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='guest')
    #     new = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='director')

    #     result = user_services.promote_to_director(old,new,update_data_func)
    #     expected = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='director')

    #     self.assertEqual(expected,result)

    # def test_if_promoteToPlayer_works_correctly(self):
    #     update_data_func = lambda update_user_role, n: update_user_role
    #     old = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='guest')
    #     new = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='player')

    #     result = user_services.promote_to_player(old,new,update_data_func=update_data_func)
    #     expected = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='player')

    #     self.assertEqual(expected,result)

    # def test_if_refuseUser_works_correctly(self):
    #     update_data_func = lambda update_user_role, n: update_user_role
    #     old = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='pending approval')
    #     new = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='guest')

    #     result = user_services.refuse_user_by_id(old,new,update_data_func=update_data_func)
    #     expected = Users(user_id = 1, email= 'test@gmail.com', password = '123', user_role='guest')

    #     self.assertEqual(expected,result)
