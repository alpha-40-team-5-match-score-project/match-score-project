import unittest
from unittest.mock import Mock
from server.data.models import Tournaments, Tournament_registration
from server.services import tournament_services


def fake_tournament(
    id=1,
    title="Test Championship",
    location="Test Country",
    start_date="2022-11-30",
    end_date="2022-12-10",
    tournament_types_id=1,
    prize=1,
    tournament_winner="Test Winner",
):
    mock_tournament = Mock(spec=Tournaments)
    mock_tournament.id = id
    mock_tournament.title = title
    mock_tournament.location = location
    mock_tournament.start_date = start_date
    mock_tournament.end_date = end_date
    mock_tournament.tournament_types_id = tournament_types_id
    mock_tournament.prize = prize
    mock_tournament.tournament_winner = tournament_winner

    return mock_tournament


class TournamentService_Should(unittest.TestCase):
    def test_createTournament_return_matchWithGeneratedId(self):
        generated_id = 1
        insert_data_func = lambda q, tournament: generated_id

        result = tournament_services.create_tournament(
            Tournaments(
                id=1,
                title="Test Championship",
                location="Test Country",
                start_date="2022-11-30",
                end_date="2022-12-10",
                tournament_types_id=1,
                prize=1,
                tournament_winner="Test Winner",
            ),
            insert_data_func,
        )
        expected = Tournaments(
            id=1,
            title="Test Championship",
            location="Test Country",
            start_date="2022-11-30",
            end_date="2022-12-10",
            tournament_types_id=1,
            prize=1,
            tournament_winner="Test Winner",
        )

        self.assertEqual(expected, result)

    def test_findTournament_by_id_works_correctly(self):
        get_data_func = lambda q, id: [
            (1, "Test Championship", "Test Country", "2022-11-30", "2022-12-10", 1, 1)
        ]

        result = tournament_services.get_by_id(1, get_data_func=get_data_func)
        expected = Tournaments(
            id=1,
            title="Test Championship",
            location="Test Country",
            start_date="2022-11-30",
            end_date="2022-12-10",
            tournament_types_id=1,
            prize=1,
        )

        self.assertEqual(expected, result)

    def test_ShowAllTournaments_return_data_correctly(self):
        get_data_func = lambda q: (
            (1, "Test Championship", "Test Country", "2022-11-30", "2022-12-10", 1, 1),
            (
                2,
                "Another Test Championship",
                "Another Test Country",
                "2022-12-10",
                "2022-12-20",
                2,
                2,
            ),
        )

        result = tournament_services.all(get_data_func=get_data_func)
        expected = [
            Tournaments(
                id=1,
                title="Test Championship",
                location="Test Country",
                start_date="2022-11-30",
                end_date="2022-12-10",
                tournament_types_id=1,
                prize=1,
            ),
            Tournaments(
                id=2,
                title="Another Test Championship",
                location="Another Test Country",
                start_date="2022-12-10",
                end_date="2022-12-20",
                tournament_types_id=2,
                prize=2,
            ),
        ]

        self.assertEqual(expected, [test for test in result])

    def test_checkPlayer_by_name_works_correctly(self):
        get_data_func = lambda id, name: [(1, "Test Player")]

        result = tournament_services.check_player_name(
            1, "Test Player", get_data_func=get_data_func
        )
        expected = Tournament_registration(tournament_id=1, player_name="Test Player")

        self.assertEqual(expected, result)

    def test_insertPlayer_works_correctly(self):
        generated_id = 1
        insert_data_func = lambda q, player: generated_id

        result = tournament_services.insert_players_in_tournament(
            Tournament_registration(
                id=1, tournament_id=1, player_name="Test Player", player_total_score=0
            ),
            insert_data_func=insert_data_func,
        )
        expected = Tournament_registration(
            id=1, tournament_id=1, player_name="Test Player", player_total_score=0
        )

        self.assertEqual(expected, result)
