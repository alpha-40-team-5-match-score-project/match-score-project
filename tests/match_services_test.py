import unittest
from unittest.mock import Mock
from server.data.models import Matches, Tournament_registration
from server.services import match_services
from server.common.response import Unauthorized


def fake_match(
    match_id=1,
    round=2,
    date="2022-11-30",
    tournament_id=1,
    score_one=0,
    score_two=0,
    player_one="Ivan",
    player_two="Stoyan",
):
    mock_match = Mock(spec=Matches)
    mock_match.match_id = match_id
    mock_match.round = round
    mock_match.date = date
    mock_match.tournament_id = tournament_id
    mock_match.score_one = score_one
    mock_match.score_two = score_two
    mock_match.player_one = player_one
    mock_match.player_two = player_two

    return mock_match


class MatchService_Should(unittest.TestCase):
    def test_shuffle_players_works_correctly(self):
        get_data_func = lambda q, x: [
            (1, "Ivan"),
            (2, "Dragan"),
            (3, "Petkan"),
            (4, "Stoyan"),
        ]

        result = match_services.shuffle_players(1, get_data_func=get_data_func)

        self.assertEqual(6, len(result))

    def test_shuffle_players_returnEmptyList_when_no_players(self):
        get_data_func = lambda q, x: []

        result = match_services.shuffle_players(1, get_data_func=get_data_func)

        self.assertEqual([], result)

    def test_shuffle_players_returnError_when_players_not_even(self):
        get_data_func = lambda q, x: [(1, "ivan")]

        result = type(match_services.shuffle_players(1, get_data_func=get_data_func))
        expected = Unauthorized

        self.assertEqual(expected, result)

    def test_createLeague_return_matchWithGeneratedId(self):
        generated_id = 1
        insert_data_func = lambda q, matches: generated_id

        result = match_services.create_league_match(
            Matches(
                round=2,
                date="2022-11-30",
                tournament_id=1,
                score_one=0,
                score_two=0,
                player_one="Ivan",
                player_two="Stoyan",
            ),
            insert_data_func,
        )
        expected = Matches(
            round=2,
            date="2022-11-30",
            tournament_id=1,
            score_one=0,
            score_two=0,
            player_one="Ivan",
            player_two="Stoyan",
        )

        self.assertEqual(expected, result)

    def test_createKnockout_return_matchWithGeneratedId(self):
        generated_id = 1
        insert_data_func = lambda q, matches: generated_id

        result = match_services.create_knockout_match(
            Matches(
                round=2,
                date="2022-11-30",
                tournament_id=1,
                score_one=0,
                score_two=0,
                player_one="Ivan",
                player_two="Stoyan",
            ),
            insert_data_func,
        )
        expected = Matches(
            round=2,
            date="2022-11-30",
            tournament_id=1,
            score_one=0,
            score_two=0,
            player_one="Ivan",
            player_two="Stoyan",
        )

        self.assertEqual(expected, result)

    def test_findPlayerEmail_return_email_correctly(self):
        get_data_func = lambda q, id: ("test_email@gmail.com")

        result = match_services.find_player_email_to_send(
            1, get_data_func=get_data_func
        )
        expected = "test_email@gmail.com"

        self.assertEqual(expected, result)

    def test_findTournament_by_id_works_correctly(self):
        get_data_func = lambda q, id_one: [
            (1, 2, "2022-11-30", 1, 0, 0, "Ivan", "Stoyan", "Ivan")
        ]

        result = match_services.find_tournament_by_id(1, 2, get_data_func=get_data_func)
        expected = Matches(
            match_id=1,
            round=2,
            date="2022-11-30",
            tournament_id=1,
            score_one=0,
            score_two=0,
            player_one="Ivan",
            player_two="Stoyan",
            winner="Ivan",
        )

        self.assertEqual(expected, result)

    def test_winner_distribution_reutrn_unauthorized_correctly_when_player_not_even(
        self,
    ):
        get_data_func = lambda q, id: [("Ivan")]

        result = type(
            match_services.winner_distribution(1, 1, get_data_func=get_data_func)
        )
        expected = Unauthorized

        self.assertEqual(expected, result)

    def test_insertRoundWinner_works_correctly(self):
        generated_id = 1
        insert_data_func = lambda q, matches: generated_id

        result = match_services.insert_round_winners(
            Matches(
                round=2,
                date="2022-11-30",
                tournament_id=1,
                score_one=0,
                score_two=0,
                player_one="Ivan",
                player_two="Stoyan",
            ),
            insert_data_func,
        )
        expected = Matches(
            round=2,
            date="2022-11-30",
            tournament_id=1,
            score_one=0,
            score_two=0,
            player_one="Ivan",
            player_two="Stoyan",
        )

        self.assertEqual(expected, result)

    def test_startMatches_return_update_matche_correctly(self):
        generated_id = 1
        update_data_func = lambda q, id: generated_id
        old_match = Matches(
            match_id=1,
            round=1,
            date="2022-11-30",
            tournament_id=1,
            score_one=0,
            score_two=0,
            player_one="Ivan",
            player_two="Stoyan",
        )
        new_match = Matches(
            match_id=1,
            round=1,
            date="2022-11-30",
            tournament_id=1,
            score_one=2,
            score_two=1,
            player_one="Ivan",
            player_two="Stoyan",
        )

        result = match_services.start_matches(
            old_match, new_match, update_data_func=update_data_func
        )

        expected = Matches(
            match_id=1,
            round=1,
            date="2022-11-30",
            tournament_id=1,
            score_one=2,
            score_two=1,
            player_one="Ivan",
            player_two="Stoyan",
        )

        self.assertEqual(result, expected)

    def test_inserKnockoutWinner_return_value_correctly(self):
        generated_id = 1
        insert_data_func = lambda q, matches: generated_id

        result = match_services.insert_knockout_winner(
            Matches(
                round=1,
                date="2022-11-30",
                tournament_id=1,
                score_one=0,
                score_two=0,
                player_one="Ivan",
                player_two="Stoyan",
            ),
            "Ivan",
            "Stoyan",
            insert_data_func=insert_data_func,
        )
        expected = (
            Matches(
                match_id=1,
                round=1,
                date="2022-11-30",
                tournament_id=1,
                score_one=0,
                score_two=0,
                player_one="Ivan",
                player_two="Stoyan",
            ),
            1,
        )
        self.assertEqual(expected, result)

    def test_checkToProceed_return_correctly_when_not_None(self):
        get_data_func = lambda q, id: [("Ivan"), ("Stoyan")]

        result = match_services.check_to_proceed_next_round(
            "2", get_data_func=get_data_func
        )
        expected = ["Ivan", "Stoyan"]

        self.assertEqual(expected, result)

    def test_check_ToProceed_return_emptyList_when_None(self):
        get_data_func = lambda q, id: []

        result = match_services.check_to_proceed_next_round(
            "2", get_data_func=get_data_func
        )
        expected = []

        self.assertEqual(expected, result)

    def test_insertRoundWinner_works_correctly(self):
        generated_id = 1
        insert_data_func = lambda q, matches: generated_id

        result = match_services.insert_round_winners(
            Matches(
                match_id=1,
                round=1,
                date="2022-11-30",
                tournament_id=1,
                score_one=0,
                score_two=0,
                player_one="Ivan",
                player_two="Stoyan",
            ),
            insert_data_func=insert_data_func,
        )
        expected = Matches(
            match_id=1,
            round=1,
            date="2022-11-30",
            tournament_id=1,
            score_one=0,
            score_two=0,
            player_one="Ivan",
            player_two="Stoyan",
        )

        self.assertEqual(expected, result)

    def test_TournamentStanding_return_data_correctly(self):
        get_data_func = lambda q, id: [(1, 1, "Ivan", 2)]

        result = match_services.tournament_standings(1, get_data_func=get_data_func)
        expected = [
            Tournament_registration(
                id=1, tournament_id=1, player_name="Ivan", player_total_score=2
            )
        ]

        self.assertEqual(expected, [test for test in result])

    def test_TournamentStanding_return_empty_list_when_data_not_present(self):
        get_data_func = lambda q, id: []

        result = match_services.tournament_standings(1, get_data_func=get_data_func)

        self.assertEqual([], [test for test in result])

    def test_CheckLeagueFinished_return_data_correctly(self):
        get_data_func = lambda q, id: ("Ivan")

        result = match_services.check_is_league_finished_or_not(
            1, get_data_func=get_data_func
        )
        expected = "Ivan"

        self.assertEqual(expected, result)

    def test_CheckKnockoutFinished_return_data_correctly(self):
        get_data_func = lambda q, id: ("Ivan")

        result = match_services.check_is_knockout_finished_or_not(
            1, get_data_func=get_data_func
        )
        expected = "Ivan"

        self.assertEqual(expected, result)
