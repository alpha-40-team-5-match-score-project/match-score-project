import unittest
from server.data.models import Players, Tournament_registration, SportClubs
from server.services import player_services


class MatchService_Should(unittest.TestCase):
    def test_ShowAllPlayers_return_data_correctly(self):
        get_data_func = lambda q: (
            ("Ivan", "BG", 1, 10, 2, 3, 3),
            ("Stoyan", "BG", 1, 8, 2, 2, 4),
        )

        result = player_services.show_all_players(get_data_func=get_data_func)
        expected = [
            Players(
                name="Ivan",
                country_code="BG",
                sport_club_id=1,
                match_played=10,
                match_won=2,
                tournament_played=3,
                tournament_won=3,
            ),
            Players(
                name="Stoyan",
                country_code="BG",
                sport_club_id=1,
                match_played=8,
                match_won=2,
                tournament_played=2,
                tournament_won=4,
            ),
        ]

        self.assertEqual(expected, [test for test in result])

    def test_GetPlayerInformation_returns_data_correctly(self):
        get_data_func = lambda q, id: [("Ivan", "BG", 1, 10, 2, 3, 3)]

        result = player_services.get_player_information(
            "Ivan", get_data_func=get_data_func
        )
        expected = Players(
            name="Ivan",
            country_code="BG",
            sport_club_id=1,
            match_played=10,
            match_won=2,
            tournament_played=3,
            tournament_won=3,
        )

        self.assertEqual(expected, result)

    def test_GetPlayerInformation_returns_None_when_no_data(self):
        get_data_func = lambda q, id: []
        result = player_services.get_player_information(
            "Ivan", get_data_func=get_data_func
        )

        self.assertEqual(None, result)

    def test_manualCreation_works_correctly(self):
        insert_data_func = lambda player, ins: player

        result = player_services.manual_player_creation(
            Players(name="Ivan"), insert_data_func=insert_data_func
        )
        expected = Players(name="Ivan")

        self.assertEqual(expected, result)

    def test_createPlayerInfo_works_correctly(self):
        insert_data_func = lambda player, ins: player

        result = player_services.create_player_info(
            Tournament_registration(id=1, tournament_id=1, player_name="Ivan"),
            insert_data_func=insert_data_func,
        )
        expected = Tournament_registration(id=1, tournament_id=1, player_name="Ivan")

        self.assertEqual(expected, result)

    def test_updatePlayer_works_correctly(self):
        update_data_func = lambda update_player, n: update_player
        old = Players(name="Ivan")
        new = Players(name="Ivan", country_code="BG", sport_club_id=1)

        result = player_services.update_player_info(
            old, new, update_data_func=update_data_func
        )
        expected = Players(name="Ivan", country_code="BG", sport_club_id=1)

        self.assertEqual(expected, result)

    def test_createSportClub_works_correctly(self):
        generated_club_id = 1
        insert_data_func = lambda q, id: generated_club_id

        result = player_services.create_sport_club(
            SportClubs(name="UEFA", creation_date="2022-11-30"),
            insert_data_func=insert_data_func,
        )
        expected = SportClubs(id=1, name="UEFA", creation_date="2022-11-30")

        self.assertEqual(expected, result)

    def test_getPlayerEmail_return_data_correctly(self):
        get_data_func = lambda q, name: "test_email@gmail.com"

        result = player_services.get_email_by_player_name(
            "Ivan", get_data_func=get_data_func
        )
        expected = "test_email@gmail.com"

        self.assertEqual(expected, result)
