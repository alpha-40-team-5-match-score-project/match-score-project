<img src="https://i.postimg.cc/9XDL91xS/5-Activities-To-Lead-a-More-Active-Lifestyle-1024x400.jpg" alt="Job_Lander_logo" width="1024px" style="margin-top: 20px;"/>

# Telerik Academy Match Point Project

|Team Members |
|--|
|[Niki Vedzhov](https://gitlab.com/Niksomus) |
|[Mitko Bochev](https://gitlab.com/mitkobochev) |


## [](#gitlab-link-to-main-files)GitLab Link to main files

Main files link: https://gitlab.com/alpha-40-team-5-match-score-project/match-score-project

## [](#tools-used-for-web-teamwork-project)Tools used for Web Teamwork Project

- Framework - Fast API
- Postman - for building and using Fast API
- MariaDB - for the creation of a working database
- Microsoft Workbench - for the illustration and editing of used database
- MailJet - for the emailing included in the system

----------------------------------------------------------------------------------------------------------------------------------------------

## [](#description)Description
Design and implement a organization and management of sport events. MatchScore aims to solve that problem by providing all the tools required.
The Match Score project provides a RESTful API, which can be used by all kinds of clients, interested in the application.

## [](#database-schema)Database Schema

<img src="https://i.postimg.cc/fLdym615/img-database-match-score-team-5.png" alt="Job_Lander_logo" width="1024px" style="margin-top: 20px;"/>

## [](#features)Features:
----------------------------------------------------------------------------------------------------------------------------------------------

- Organizing events
    - Match – a one-off event
    - Tournament – knockout or league format

- Managing events
    - Mark the score
    - Change the date
    - Update the participants

- Player profile
    - Name, Country, Sports Club

- Score tracking
    - Tournament and player statistics

- Registration
    - Allows event organization and management (if approved)
    - Associate with player profile

## [](#project-functionalities)Project functionalities
----------------------------------------------------------------------------------------------------------------------------------------------
All project functionalities are described as doc strings on their respective pages and rows. Below you can view all the implemented functions of the project.

## [](#match)Match
----------------------------------------------------------------------------------------------------------------------------------------------

- When it will be played
- Participants – at least two
- Match format
    - Score limited – (e.g., first to 9 points)

## [](#tournament)Tournament
----------------------------------------------------------------------------------------------------------------------------------------------

- Participants
- Title
- Matches that are part of it
- Format
    - Knockout
    - League – scoring for loss, draw and, win
- Tournament match format
- Prize for top finishers

## [](#player-profiles)Player Profiles
----------------------------------------------------------------------------------------------------------------------------------------------

- Full Name (only letters, spaces, dashes)
- Country
- Sports Club

## [](#registered-users)Registered Users
----------------------------------------------------------------------------------------------------------------------------------------------

- Email
- Password
- Option to associate with a player profile

### [](#tournament-creation)Tournament Creation
----------------------------------------------------------------------------------------------------------------------------------------------
- Create a knockout tournament
    - The system generates a tournament scheme and **randomly** assigns participants to matches
    - The system tries to link any of the four participants to an **existing profile** by their full name. If a profile is not found, one will be **automatically** created with no country and no sports club.
    -  The system automatically updates the next match when a new score is added
    
- Create a league tournament with a scoring system that is 0 pts for a loss, 1 for a draw, 2 for a win.
    - The system calculates that at least three rounds are required and randomly creates matches for each round
    - After the first few rounds, the intermediate scoring standings by points can be checked mid-tournament.
    - Final rankings can be seen after the league tournament has been finished, to determine to tournament winner.

### [](#manage-a-player-profile)Manage a player profile
----------------------------------------------------------------------------------------------------------------------------------------------

- Automatic creation of a player – when creating a tournament, if a participant cannot be matched to an existing profile
- Manual creation – by admin or director
- Directors can edit any player profile if not linked to a user
- If linked to a user, only they can edit it

### [](#registration)Registration
----------------------------------------------------------------------------------------------------------------------------------------------

- Anyone can register with an email **(unique in the system)** and a password

- After registration
    - Promote-to-director request – if approved by **admin**, the registered user can organize and manage tournaments
    - Link-profile request – if approved by **admin**, the player profile is associated with the registered user

- Receives email notification when added to a tournament/match

### [](#administration)Administration
----------------------------------------------------------------------------------------------------------------------------------------------

- Admin user is predefined in the system
- Can view and approve or decline promote-to-director and link-profile requests
- Can manage any resource – CRU

### [](#third-party-service)Third-Party Service
----------------------------------------------------------------------------------------------------------------------------------------------

- Integrated with https://dev.mailjet.com/email/guides/send-api-v31/ for email notifications
- Notifications:
    - Promote-to-director request approved/declined
    - Link-profile request approved/declined
    - Added to tournament
    - Added to match

### [](#score-and-tournament-tracking)Score and Tournament Tracking
----------------------------------------------------------------------------------------------------------------------------------------------

- Information is available without any authentication
- View any past, present or future tournament
- View any match
- View any player profile
    - Tournaments played / won
    - Matches played / won