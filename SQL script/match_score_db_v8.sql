-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema match_score_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema match_score_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `match_score_db` DEFAULT CHARACTER SET utf8mb3 ;
USE `match_score_db` ;

-- -----------------------------------------------------
-- Table `match_score_db`.`countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score_db`.`countries` (
  `code` VARCHAR(3) NOT NULL,
  `country_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`code`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `match_score_db`.`tournament_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score_db`.`tournament_types` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tournament_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `match_score_db`.`prizes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score_db`.`prizes` (
  `prize_id` INT(11) NOT NULL AUTO_INCREMENT,
  `prize_name` VARCHAR(45) NOT NULL,
  `prize_money` INT(11) NOT NULL,
  PRIMARY KEY (`prize_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `match_score_db`.`tournament`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score_db`.`tournament` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NOT NULL,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `tournament_types_id` INT(11) NOT NULL,
  `prize` INT(11) NOT NULL,
  `tournament_winner` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Tournament_Tournament_types1_idx` (`tournament_types_id` ASC) VISIBLE,
  INDEX `fk_tournament_prizes1_idx` (`prize` ASC) VISIBLE,
  CONSTRAINT `fk_Tournament_Tournament_types1`
    FOREIGN KEY (`tournament_types_id`)
    REFERENCES `match_score_db`.`tournament_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournament_prizes1`
    FOREIGN KEY (`prize`)
    REFERENCES `match_score_db`.`prizes` (`prize_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `match_score_db`.`sport_clubs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score_db`.`sport_clubs` (
  `club_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `creation_date` DATE NOT NULL,
  PRIMARY KEY (`club_id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `match_score_db`.`players`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score_db`.`players` (
  `name` VARCHAR(45) NOT NULL,
  `country_code` VARCHAR(3) NULL DEFAULT NULL,
  `sport_club_id` INT(11) NULL DEFAULT NULL,
  `match_won` INT(11) NULL DEFAULT NULL,
  `match_played` INT(11) NULL DEFAULT NULL,
  `tournament_played` INT(11) NULL DEFAULT NULL,
  `tournament_won` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`name`),
  INDEX `fk_Players_countries1_idx` (`country_code` ASC) VISIBLE,
  INDEX `fk_Players_sport_clubs1_idx` (`sport_club_id` ASC) VISIBLE,
  CONSTRAINT `fk_Players_countries1`
    FOREIGN KEY (`country_code`)
    REFERENCES `match_score_db`.`countries` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Players_sport_clubs1`
    FOREIGN KEY (`sport_club_id`)
    REFERENCES `match_score_db`.`sport_clubs` (`club_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `match_score_db`.`matches`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score_db`.`matches` (
  `match_id` INT(11) NOT NULL AUTO_INCREMENT,
  `round` INT(11) NOT NULL DEFAULT 0,
  `date` DATE NOT NULL,
  `tournament_id` INT(11) NOT NULL,
  `score_one` INT(11) NULL DEFAULT 0,
  `score_two` INT(11) NULL DEFAULT 0,
  `player_one` VARCHAR(45) NOT NULL,
  `player_two` VARCHAR(45) NOT NULL,
  `winner` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`match_id`),
  INDEX `fk_Matches_Tournament1_idx` (`tournament_id` ASC) VISIBLE,
  INDEX `fk_matches_players1_idx` (`player_one` ASC) VISIBLE,
  INDEX `fk_matches_players2_idx` (`player_two` ASC) VISIBLE,
  CONSTRAINT `fk_Matches_Tournament1`
    FOREIGN KEY (`tournament_id`)
    REFERENCES `match_score_db`.`tournament` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matches_players1`
    FOREIGN KEY (`player_one`)
    REFERENCES `match_score_db`.`players` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matches_players2`
    FOREIGN KEY (`player_two`)
    REFERENCES `match_score_db`.`players` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 875
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `match_score_db`.`tournament_registration`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score_db`.`tournament_registration` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` INT(11) NOT NULL,
  `player_name` VARCHAR(45) NOT NULL,
  `player_total_score` INT(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_tournament_registration_tournament1_idx` (`tournament_id` ASC) VISIBLE,
  INDEX `fk_tournament_registration_players1_idx` (`player_name` ASC) VISIBLE,
  CONSTRAINT `fk_tournament_registration_players1`
    FOREIGN KEY (`player_name`)
    REFERENCES `match_score_db`.`players` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournament_registration_tournament1`
    FOREIGN KEY (`tournament_id`)
    REFERENCES `match_score_db`.`tournament` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 106
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `match_score_db`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score_db`.`users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `user_role` VARCHAR(45) NOT NULL,
  `player_name` VARCHAR(45) NULL DEFAULT NULL,
  `is_approved` TINYINT(4) NULL DEFAULT 0,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_users_players1_idx` (`player_name` ASC) VISIBLE,
  CONSTRAINT `fk_users_players1`
    FOREIGN KEY (`player_name`)
    REFERENCES `match_score_db`.`players` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 18
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
