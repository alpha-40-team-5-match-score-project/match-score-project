from server.data import database
from server.data.models import Matches, Tournament_registration
from server.common.response import Unauthorized
import random
from itertools import combinations

"""
A function used to find a match by it's ID
"""


def find_match_by_id(match_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT match_id, round, date, tournament_id, score_one, score_two, player_one, player_two, winner 
            from matches where match_id = ? """,
        (match_id,),
    )

    return next(Matches.from_query_result(*row) for row in data)


"""
A function used to shuffle the players, selected from the tournament registrations
"""


def shuffle_players(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT player_name from tournament_registration where tournament_id = ?""",
        (id,),
    )
    item = random.sample(data, k=len(data))
    my_list = []
    for x in item:
        my_list.append(x[0])
    if len(my_list) % 2 == 0:
        return [(i) for i in combinations(my_list, 2)]

    else:
        return Unauthorized(f"The player in tournament must be even number!")


"""
A function used to create a league match.
Rounds are incremented by match.
"""


def create_league_match(match: Matches, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    result = shuffle_players(match.tournament_id)

    for matches in result:
        generated_id = insert_data_func(
            """INSERT INTO matches(date,tournament_id,score_one, score_two,player_one,player_two) VALUES(?,?,0,0,?,?)""",
            (match.date, match.tournament_id, matches[0], matches[1]),
        )
        match.match_id = generated_id
        update_rounds = insert_data_func(
            """UPDATE matches SET round = (SELECT max(round) 
                            from matches where tournament_id = ?) + 1 where tournament_id = ? and match_id = ?""",
            (
                match.tournament_id,
                match.tournament_id,
                match.match_id,
            ),
        )

    return match


"""
A function used to create a knockout match.
Rounds are incremented by the type of final(e.g. quarter-final, semi-finals, finals).
"""


def create_knockout_match(match: Matches, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    pairs = []
    result = shuffle_players(match.tournament_id)

    for pair in result:
        if not any(pair[0] in participants for participants in pairs) and not any(
            pair[1] in participants for participants in pairs
        ):
            pairs.append(pair)
    for matches in pairs:
        generated_id = insert_data_func(
            """INSERT INTO matches(date,tournament_id,score_one, score_two,player_one,player_two) VALUES(?,?,0,0,?,?)""",
            (match.date, match.tournament_id, matches[0], matches[1]),
        )
        match.match_id = generated_id

        update_rounds = insert_data_func(
            """UPDATE matches SET round = round + 1 where tournament_id = ? and match_id = ?""",
            (
                match.tournament_id,
                match.match_id,
            ),
        )
        update_results(pairs, match)
    return match


"""
A function used to find a player's user email, based on a players linked name.
"""


def find_player_email_to_send(tournament_id: str, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT email from users as u join tournament_registration as t on u.player_name = t.player_name where tournament_id = ? """,
        (tournament_id,),
    )

    return data


"""
A function used to update the round, based on the number of players pairs
"""


def update_results(rounds: str, match: Matches, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query
    if len(rounds) == 1:
        update_rounds = update_data_func(
            """UPDATE matches SET round = 1 where tournament_id = ? and match_id = ?""",
            (match.tournament_id, match.match_id),
        )
    elif len(rounds) == 2:
        update_rounds = update_data_func(
            """UPDATE matches SET round = 2 where tournament_id = ? and match_id = ?""",
            (match.tournament_id, match.match_id),
        )
    elif len(rounds) == 4:
        update_rounds = update_data_func(
            """UPDATE matches SET round = 3 where tournament_id = ? and match_id = ?""",
            (match.tournament_id, match.match_id),
        )
    elif len(rounds) == 8:
        update_rounds = update_data_func(
            """UPDATE matches SET round = 4 where tournament_id = ? and match_id = ?""",
            (match.tournament_id, match.match_id),
        )
    else:
        update_rounds = update_data_func(
            """UPDATE matches SET round = 5 where tournament_id = ? and match_id = ?""",
            (match.tournament_id, match.match_id),
        )


"""
A function used to check the tournament type
"""


def check_tournament_type(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT tournament_type from tournament as t join 
        tournament_types as tp on tp.id = t.tournament_types_id where t.id = ?""",
        (id,),
    )

    return data[0][0]


"""
A function used to find a tournament, based on it's ID
"""


def find_tournament_by_id(id: int, match_ids: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        """SELECT match_id,round,date,tournament_id,score_one,score_two,player_one,player_two,winner from matches
            where tournament_id = ? and match_id = ?""",
        (id, match_ids),
    )

    return next((Matches.from_query_result(*row) for row in data), None)


"""
A function used to start the matches and assign a score to both players, as well as a winner.
"""


def start_matches(old: Matches, new: Matches, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    merged = Matches(
        match_id=old.match_id,
        round=old.round,
        date=old.date,
        tournament_id=old.tournament_id,
        score_one=new.score_one,
        score_two=new.score_two,
        player_one=old.player_one,
        player_two=old.player_two,
        winner=new.winner,
    )

    update_data_func(
        """UPDATE matches SET
           score_one = ?, score_two = ?, winner = ?
           WHERE match_id = ?
        """,
        (merged.score_one, merged.score_two, merged.winner, merged.match_id),
    )
    return merged


"""
A function used to insert the winner in the database
"""


def insert_knockout_winner(
    match: Matches, player_one: str, player_two: str, insert_data_func=None
):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    generated_id = insert_data_func(
        """INSERT INTO matches(date,tournament_id,score_one, score_two,player_one, player_two) VALUES(?,?,?,?,?)""",
        (
            match.date,
            match.tournament_id,
            match.score_one,
            match.score_two,
            player_one,
            player_two,
        ),
    )
    match.match_id = generated_id

    update_rounds = insert_data_func(
        """UPDATE matches SET round = round + 1 where tournament_id =?""",
        (match.tournament_id,),
    )
    return match, update_rounds


"""
A function used to assign a winner to a specific match and distribute the points, based on if there is a winner or if the game is a tie.
It laso updates the total player score in the registrations for further statistical uses.
"""


def assign_winner(t_id: int, winner_name: str, match_id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query
    result = find_tournament_by_id(t_id, match_id)
    if result.score_one > result.score_two:
        data = update_data_func(
            """UPDATE tournament_registration set player_total_score = player_total_score + 2 where tournament_id = ? and player_name = ?""",
            (
                t_id,
                winner_name,
            ),
        )
        update_winner_column = update_data_func(
            """UPDATE matches set winner = ? where match_id = ?""",
            (
                winner_name,
                match_id,
            ),
        )
    elif result.score_two > result.score_one:
        data = update_data_func(
            """UPDATE tournament_registration set player_total_score = player_total_score + 2 where tournament_id = ? and player_name = ?""",
            (
                t_id,
                winner_name,
            ),
        )
        update_winner_column = update_data_func(
            """UPDATE matches set winner = ? where match_id = ?""",
            (
                winner_name,
                match_id,
            ),
        )
    else:
        data = update_data_func(
            """UPDATE tournament_registration set player_total_score = player_total_score+ 1 where tournament_id = ? and (player_name = ? or player_name = ?)""",
            (
                t_id,
                winner_name,
                winner_name,
            ),
        )
    return data


"""
A function used to select all winners, based on tournament ID and round
"""


def winner_distribution(id: int, rounds: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT winner from matches where tournament_id = ? and round = ? and winner is not null""",
        (
            id,
            rounds,
        ),
    )
    item = data
    my_list = []
    for x in item:
        my_list.append(x[0])
    if len(my_list) % 2 == 0:
        return [(i) for i in combinations(my_list, 2)]

    else:
        return Unauthorized(f"The player in tournament must be even number!")


"""
A function used to check if there are any unfinished matches.
If all matches have finished (have a winner) then the tournament proceeds to the next set of rounds.
"""


def check_to_proceed_next_round(rounds: str, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT winner from matches where round = ? and winner is null""", (rounds,)
    )

    return data


"""
A function used to assign the winners of a match
"""


def insert_round_winners(match: Matches, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    rounds = []
    result = winner_distribution(match.tournament_id, match.round)

    for pair in result:
        if not any(pair[0] in participants for participants in rounds) and not any(
            pair[1] in participants for participants in rounds
        ):
            rounds.append(pair)

    for matches in rounds:
        generated_id = insert_data_func(
            """INSERT INTO matches(date,tournament_id,score_one, score_two,player_one,player_two) VALUES(?,?,0,0,?,?)""",
            (match.date, match.tournament_id, matches[0], matches[1]),
        )
        match.match_id = generated_id
        update_results(rounds, match)

    return match


"""
A function used to view the current tournament standings by point earned.
"""


def tournament_standings(tournament_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT id, tournament_id, player_name, player_total_score from tournament_registration where tournament_id = ? ORDER BY player_total_score DESC""",
        (tournament_id,),
    )

    return (
        Tournament_registration(
            id=id,
            tournament_id=tournament_id,
            player_total_score=player_total_score,
            player_name=player_name,
        )
        for id, tournament_id, player_name, player_total_score in data
    )


"""
A function used to check if a league tournament is finished or not, based on if matches are finished (have a winner).
"""


def check_is_league_finished_or_not(tournament_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT winner from matches where tournament_id = ? and winner is null""",
        (tournament_id,),
    )
    return data


"""
A function used to check who the tournament winner is, based on total points earned
"""


def check_league_winner(tournament_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT player_name from tournament_registration where tournament_id = ? order by player_total_score desc limit 1""",
        (tournament_id,),
    )
    return data


"""
A function used to check if a knockout tournament is finished or not, based on match winner statuses or if it's the final round (final match).
"""


def check_is_knockout_finished_or_not(tournament_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT winner from matches where tournament_id = ? and round = 1 and winner is not null""",
        (tournament_id,),
    )

    return data


# def update_player_scores(match_played: int,match_won:int,tournament_played: int,tournament_won: int, name:str, update_data_func = None):
#     if update_data_func is None:
#         update_data_func = database.update_query

#     match_played = update_data_func(
#         """UPDATE players SET match_played = ? where name = ?""",(match_played,name,))

#     match_won = update_data_func( """UPDATE players SET  match_won = ? where name = ?""",(match_won,name,))

#     tournament_played = update_data_func( """UPDATE players SET tournament_played  = ? where name = ?""",(tournament_played,name,))

#     tournament_won = update_data_func("""UPDATE players SET tournament_won = ? where name = ?""",(tournament_won, name,))

#     return match_played, match_won, tournament_played, tournament_won

# def count_total_matches_played(player_name: str, get_data_func = None):
#     if get_data_func is None:
#         get_data_func = database.read_query

#     data = get_data_func(
#         """SELECT count(?) from players as p
#             join matches as m on m.player_one = p.name where player_one = ? or player_two = ?""",(player_name,player_name,player_name,))

#     return data

# def count_total_matches_won(player_name: str, get_data_func = None):
#     if get_data_func is None:
#         get_data_func = database.read_query

#     data = get_data_func(
#         """SELECT count(?) + 1 from matches where winner = ?""",(player_name,player_name,))
#     print(data)
#     return data

# def count_tournament_played(player_name: str, get_data_func = None):
#     if get_data_func is None:
#         get_data_func = database.read_query

#     data = get_data_func('''SELECT count(?) from tournament_registration where player_name = ?''',(player_name,player_name,))

#     return data
# def count_tournament_won(player_name: str, get_data_func = None):
#     if get_data_func is None:
#         get_data_func = database.read_query

#     data = get_data_func('''SELECT count(?) from tournament where tournament_winner = ?''',(player_name,player_name,))

#     return data
