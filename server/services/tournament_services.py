from server.data import database
from server.data.models import (
    Players,
    Tournament_registration,
    Tournaments,
    TournamentType,
)
from server.common.response import Unauthorized, BadRequest, NotFound
from server.services import player_services

"""
A function used to review all tournaments with its specific information.
"""


def all(get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT t.id, title, location, start_date, end_date,tp.tournament_type, prize from tournament as t
        join tournament_types as tp on tp.id = t.tournament_types_id"""
    )

    return (Tournaments.from_query_result(*row) for row in data)


"""
A function used to review a single tournaments with its specific information, based on its ID
"""


def get_by_id(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        """SELECT t.id, title, location, start_date, end_date,tp.tournament_type, prize from tournament as t
            join tournament_types as tp on tp.id = t.tournament_types_id where t.id = ?""",
        (id,),
    )
    if data is None or data == []:
        return NotFound("No such tournament found!")

    return next((Tournaments.from_query_result(*row) for row in data), None)


"""
A function used to create a tournament
"""


def create_tournament(tournament: Tournaments, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    generated_id = insert_data_func(
        """INSERT INTO tournament(title,location,start_date,end_date,tournament_types_id,prize) VALUES(?,?,?,?,?,?)""",
        (
            tournament.title,
            tournament.location,
            tournament.start_date,
            tournament.end_date,
            tournament.tournament_types_id,
            tournament.prize,
        ),
    )

    tournament.id = generated_id

    return tournament


"""
A function used to insert players into a specific (already existing) tournament
"""


def insert_players_in_tournament(
    players: Tournament_registration, insert_data_func=None
):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    generated_id = insert_data_func(
        """INSERT INTO tournament_registration(tournament_id, player_name) VALUES(?,?)""",
        (players.tournament_id, players.player_name),
    )

    players.id = generated_id

    return players


"""
A function used to check if a player exists.
"""


def check_if_player_exist_else_create_new(name: str, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT player_id, name, country_code, sport_club_id from players where name = ?""",
        (name,),
    )

    return (Players.from_query_result(*row) for row in data)


"""
A function used to check if a player exists, based on player name
"""


def check_player_name(player_name: str, t_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        """SELECT tournament_id, player_name from tournament_registration
            where player_name = ? and tournament_id = ? """,
        (
            player_name,
            t_id,
        ),
    )
    return next(
        (
            Tournament_registration(tournament_id=t_id, player_name=player_name)
            for t_id, player_name in data
        ),
        None,
    )


"""
A function used to update the tournament winner, based on total points earned.
"""


def update_tournament_winner(
    winner_name: str, tournament_id: int, update_data_func=None
):
    if update_data_func is None:
        update_data_func = database.update_query

    update_winner = update_data_func(
        """UPDATE tournament SET tournament_winner = ? where id = ?""",
        (winner_name, tournament_id),
    )

    return update_winner
