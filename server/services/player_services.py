from server.data import database
from server.data.models import Players, Tournament_registration, SportClubs
from server.common.response import NotFound

"""
A function used to select and review all players in the system and show their player statistics.
"""


def show_all_players(get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT name,country_code, sport_club_id ,(SELECT count(winner) from matches), 
		(SELECT count(match_id) from matches), 
        (SELECT count("Rafael Nadal") from tournament_registration),
        (SELECT count(tournament_winner) from tournament)
        from players"""
    )
    return (Players.from_query_result(*row) for row in data)


"""
A function used to select and a single players in the system and show their player statistics.
"""


def get_player_information(player_name: str, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT name,country_code, sport_club_id ,(SELECT count(winner) from matches as m where tournament_id = 6 and winner = ?), 
		(SELECT count(match_id) from matches where tournament_id = 6 and player_one = ? or player_two = ?), 
        (SELECT count("Rafael Nadal") from tournament_registration where player_name = ?),
        (SELECT count(tournament_winner) from tournament where tournament_winner = ?)
        from players where name = ? """,
        (
            player_name,
            player_name,
            player_name,
            player_name,
            player_name,
            player_name,
        ),
    )

    return next((Players.from_query_result(*row) for row in data), None)


"""
A function used to manually create a player profile
"""


def manual_player_creation(player: Players, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    generated_player = insert_data_func(
        "insert into players(name, country_code, sport_club_id) values(?,?,?)",
        (player.name, player.country_code, player.sport_club_id),
    )

    return player


"""
A function used to automatically create a player profile
"""


def create_player_info(player: Tournament_registration, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    generated_player = insert_data_func(
        "insert into players(name) values(?)", (player.player_name,)
    )

    return player


"""
A function used to update a players profile
"""


def update_player_info(old: Players, new: Players, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    merged = Players(
        name=old.name,
        country_code=new.country_code or old.country_code,
        sport_club_id=new.sport_club_id or old.sport_club_id,
    )
    update_data_func(
        """UPDATE players SET
           country_code=?,sport_club_id = ?
           WHERE name = ? 
        """,
        (
            merged.country_code,
            merged.sport_club_id,
            merged.name,
        ),
    )

    return merged


"""
A function used to manually create a sport club
"""


def create_sport_club(club: SportClubs, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    generated_club_id = insert_data_func(
        "insert into sport_clubs(name, creation_date) values(?,?)",
        (club.name, club.creation_date),
    )

    club.id = generated_club_id

    return club


"""
A function used to get a players email address, based on the player's name
"""


def get_email_by_player_name(player_name: str, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT email from users as u join players as p on u.player_name = p.name  where name = ? """,
        (player_name,),
    )

    return data
