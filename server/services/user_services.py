from server.data import database
from server.data.models import Users, Role
from mariadb import IntegrityError
from server.common.response import BadRequest, NotFound

"""
A function used to find a user, based on the users email
"""


def find_by_email(email: str, get_data_func=None) -> Users | None:
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        """SELECT user_id, email, password, user_role, player_name FROM users WHERE email = ?""",
        (email,),
    )

    return next((Users.from_query_result(*row) for row in data), None)


"""
A function used to find a user, based on the users ID
"""


def find_by_id(user_id: int, get_data_func=None) -> Users | None:
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """SELECT user_id, email, password, user_role, player_name FROM users WHERE user_id = ?""",
        (user_id,),
    )

    return next((Users.from_query_result(*row) for row in data), None)


"""
A function used to find a user, based on the users player name
"""


def find_user_by_player_name(player: str, get_data_func=None) -> Users | None:
    if get_data_func is None:
        get_data_func = database.read_query

    data = get_data_func(
        """select player_name from users where player_name = ?""", (player,)
    )

    return data or None


"""
A function used to login the user into the platform, based on a valid email and password
"""


def try_login(email: str, password: str) -> Users | None:
    user = find_by_email(email)

    return (
        user
        if user and user.password == password
        else BadRequest("Wrong email or password!")
    )


"""
A function used to create a user profile
"""


def create(email: str, password: str, insert_data_func=None) -> Users | None:
    if insert_data_func is None:
        insert_data_func = database.insert_query
    try:
        generated_id = insert_data_func(
            "INSERT INTO users(email, password, user_role) VALUES (?,?,?)",
            (email, password, Role.GUEST),
        )

        return Users(
            user_id=generated_id, email=email, password="", user_role=Role.GUEST
        )

    except IntegrityError:
        return None


"""
A function used to request a promotion to a director role
"""


def request_promotion_to_director(id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    data_user_role = update_data_func(
        """UPDATE users SET user_role = 'pending approval' where user_id = ? """, (id,)
    )

    return data_user_role


"""
A function used to request a promotion to a player role
"""


def request_promotion_to_player(new: Users, old: Users, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    merged = Users(
        user_id=old.user_id,
        email=old.email,
        password=old.password,
        user_role=new.user_role or old.user_role,
        player_name=new.player_name or old.player_name,
    )
    update_data_func(
        """UPDATE users SET
           user_role = 'pending approval', player_name=?
           WHERE user_id = ? 
        """,
        (
            merged.player_name,
            merged.user_id,
        ),
    )
    return merged


"""
A function used to accept and promote a user to a player
"""


def promote_to_player(id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    data = update_data_func(
        "UPDATE users SET is_approved = True where user_id = ?", (id,)
    )

    data_user_role = update_data_func(
        """UPDATE users SET user_role = 'player' where user_id = ? """, (id,)
    )
    return data, data_user_role


"""
A function used to accept and promote a user to a director
"""


def promote_to_director(id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    data_user_role = update_data_func(
        """UPDATE users SET user_role = 'director' where user_id = ? """, (id,)
    )
    return data_user_role


"""
A function used to refuse and demote a user to a guest and delete any player info
"""


def refuse_user_by_id(id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    data = update_data_func(
        "UPDATE users SET is_approved = False where user_id = ?", (id,)
    )

    data_user_role = update_data_func(
        """UPDATE users SET user_role = 'guest' where user_id = ? """, (id,)
    )

    data_player_name = update_data_func(
        """UPDATE users SET player_name = null where user_id = ? """, (id,)
    )
    return data, data_user_role, data_player_name
