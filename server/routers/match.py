from fastapi import APIRouter, Header
from server.data.models import Matches
from server.common import emails
from server.services import match_services, tournament_services
from server.common.response import Unauthorized, BadRequest, NotFound
from server.common.auth import get_user_or_raise_401

match_router = APIRouter(prefix="/matches")

"""
Endpoint is used to find a match by it's match ID
"""


@match_router.get("/{match_id}")
def get_match_by_id(match_id: int):
    return match_services.find_match_by_id(match_id) or NotFound(
        "Match with this ID may not exist!"
    )


"""
Endpoint to create a match, based on the tournament type (knockout/league) and send an e-mail notification to all players with a linked user profile, that they have a match assigned.
"""


@match_router.post("/")
def create_matches(match: Matches, x_token=Header()):
    try:
        member = get_user_or_raise_401(x_token)
        if (
            match_services.check_tournament_type(match.tournament_id) == "league"
            and member.is_admin()
            or member.is_director()
        ):
            check_if_player_has_email = match_services.find_player_email_to_send(
                match.tournament_id
            )
            for x in check_if_player_has_email:
                emails.register_match_send_email(x[0])
            return match_services.create_league_match(match)
        elif (
            match_services.check_tournament_type(match.tournament_id) == "knockout"
            and member.is_admin()
            or member.is_director()
        ):
            check_if_player_has_email = match_services.find_player_email_to_send(
                match.tournament_id
            )
            for x in check_if_player_has_email:
                emails.register_match_send_email(x[0])
            return match_services.create_knockout_match(match)
        else:
            return Unauthorized(
                f"There are only 2 types of tournaments! Please choose one of them!"
            )
    except:
        return BadRequest(
            "The request you want to sent is not possible because you don't have permission to do that!"
        )


"""
Endpoint to start the specific matches of the said tournament. It is initialized by inserting all registered players and matching them, based on the type of tournament.
The matches are then played and based on the final score between the players, a winner is assigned (winner receives 2 point for every win, while the loser gets 0 points).
It is possible for the match to end in a tie. In that case a set of 1 point is given to both players.
"""


@match_router.put("/start/{tournament_id}")
def start_match(matches: Matches, tournament_id: int, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin() or member.is_director():
        if match_services.check_to_proceed_next_round(matches.round) == []:
            return (
                f"Generating next tournament brackets!",
                match_services.insert_round_winners(matches),
            )

        if matches.tournament_id == tournament_id:
            existing_match = match_services.find_tournament_by_id(
                matches.tournament_id, matches.match_id
            )
            if existing_match is None or existing_match.round != matches.round:
                return NotFound(f"Oops! No such mach or round found!")
            else:
                result = match_services.start_matches(existing_match, matches)

                if not result.score_one >= 10 and not result.score_two >= 10:
                    if result.score_one > result.score_two:
                        match_services.assign_winner(
                            result.tournament_id, result.player_one, result.match_id
                        )
                        return (
                            f"The winner in round {result.round} is {result.player_one}"
                        )
                    elif result.score_two > result.score_one:
                        match_services.assign_winner(
                            result.tournament_id, result.player_two, result.match_id
                        )
                        return (
                            f"The winner in round {result.round} is {result.player_two}"
                        )
                    else:
                        match_services.assign_winner(
                            result.tournament_id, result.player_two, result.match_id
                        )
                        match_services.assign_winner(
                            result.tournament_id, result.player_one, result.match_id
                        )
                        return f"This round ended in a tie!"
                else:
                    return BadRequest(
                        f"The score have been exceed!The maximum score is 9!"
                    )
        else:
            return BadRequest(
                "The tournament you are trying to reach may be locked or there no such existing!"
            )
    else:
        Unauthorized("You have no permission to start matches!")


"""
Endpoint to check the result of an ongoing or finished tournament, based on it's status (in process/finished).
Scores can be tracked by a descinding order.
After a tournament has finished a tournament winner is assgined, based on points (if league) or final winner(if knockout)
"""


@match_router.get("/result/{t_id}", status_code=200)
def check_results(t_id: int):
    try:
        if match_services.check_tournament_type(t_id) == "league":
            if not match_services.check_is_league_finished_or_not(t_id):
                get_winner_name = match_services.check_league_winner(t_id)
                print(get_winner_name)
                tournament_services.update_tournament_winner(
                    get_winner_name[0][0], t_id
                )
                return (
                    f"This league tournament has finished! You can see the result below!"
                ), match_services.tournament_standings(t_id)
            else:
                return (
                    f"The league tournament is still is process! You can seee available score below."
                ), match_services.tournament_standings(t_id)
        elif match_services.check_tournament_type(t_id) == "knockout":
            if match_services.check_is_knockout_finished_or_not(t_id):
                get_winner_name = match_services.check_is_knockout_finished_or_not(t_id)
                tournament_services.update_tournament_winner(
                    get_winner_name[0][0], t_id
                )
                return f"The winner in this knockout tournament is: {match_services.check_is_knockout_finished_or_not(t_id)[0][0]}"
            else:
                return f"The knockout tournament is still is process! Come back later to check the score!"
    except:
        return NotFound(
            "Oops someting went wrong! Please check if this match really exists!"
        )
