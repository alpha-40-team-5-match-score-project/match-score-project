from fastapi import APIRouter, Header
from server.data.models import Players, SportClubs
from server.services import user_services, player_services
from server.common import auth
from server.common.response import Unauthorized, BadRequest, NotFound
from server.common.auth import get_user_or_raise_401
from mariadb import IntegrityError

players_router = APIRouter(prefix="/players")

"""
Endpoint to retrieve all player profiles and their respective information
"""


@players_router.get("/")
def get_all_players():
    return player_services.show_all_players() or NotFound("No players found yet!")


"""
Endpoint to retrieve a specific player profile and their respective information
"""


@players_router.get("/{player_name}")
def get_player_by_name(player_name: str):
    return player_services.get_player_information(player_name) or NotFound(
        "No such player name found!"
    )


"""
Endpoint to create a player profile
"""


@players_router.post("/")
def create_player(data: Players, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    try:
        if member.is_admin() or member.is_director():
            player = player_services.manual_player_creation(data)
            return player or BadRequest(f"Player name is taken.")
        else:
            return Unauthorized(
                f"Only admins and directors can manually create players!"
            )
    except:
        return Unauthorized("You admin session has expired! Please login again.")


"""
Endpoint to create a sports club
"""


@players_router.post("/sport-club")
def create_sport_club(data: SportClubs, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    try:
        if member.is_admin() or member.is_director():
            player_services.create_sport_club(data)
            return f"Sport Club created!"
        else:
            return Unauthorized(f"Only admins and directors can create sport clubs!")
    except:
        return Unauthorized(
            "You session has expired or the name of the club already exists!"
        )


"""
Endpoint used to update a player profile. 
If a player is associated with a user profile, only that said user can edit it's information.
Otherwise an admin or a director can edit the player profiles as well.
"""


@players_router.put("/")
def update_profile(player: Players, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    try:
        existing_player = player_services.get_player_information(player.name)
        user_player_name = player_services.get_player_information(member.player_name)
        existing_user = user_services.find_user_by_player_name(player.name)
        if (
            member.player_name is not None
            and member.player_name == player.name
            and not member.is_director()
        ):
            player_services.update_player_info(user_player_name, player)
            return f"Player info has been updated."
        elif member.is_director() and existing_user is None:
            player_services.update_player_info(existing_player, player)
            return f"Director has updated player info."
        elif member.is_admin():
            player_services.update_player_info(existing_player, player)
            return f"Admin has updated player info."
        else:
            return Unauthorized(
                "You have to be logged in to change your player info or user is linked to player profile."
            )
    except:
        return Unauthorized("You admin session has expired! Please login again.")
