from fastapi import APIRouter, Header
from server.data.models import Tournament_registration, Tournaments
from server.services import tournament_services, player_services
from server.data.models import Tournaments
from server.common import emails
from server.common.response import Unauthorized, BadRequest, NotFound
from server.common.auth import get_user_or_raise_401

tournmanet_router = APIRouter(prefix="/tournament")

"""
Endpoint used to retrieve all tournaments and their respective information
"""


@tournmanet_router.get("/")
def view_all_tournaments():
    return tournament_services.all() or NotFound("No tournament still exists!")


"""
Endpoint used to retrieve a specific tournament and its respective information
"""


@tournmanet_router.get("/{tournament_id}", status_code=200)
def get_by_id(tournament_id: int):
    result = tournament_services.get_by_id(tournament_id)

    return result or NotFound("No such tournament record found!")


"""
Endpoint used to create a tournament
"""


@tournmanet_router.post("/", status_code=201)
def create(tournament: Tournaments, x_token=Header()):
    player = get_user_or_raise_401(x_token)
    try:
        if (
            player.is_admin()
            or player.is_director()
            and tournament.start_date > tournament.end_date
        ):
            return tournament_services.create_tournament(tournament)
        else:
            return Unauthorized(
                "Only directors and admin can add players in tournament!"
            )

    except SyntaxError:
        return Unauthorized("You admin session has expired! Please login again.")


"""
Endpoint used to insert the registered players into a tournament.
If a player does not exist, but is inserted by a director/admin in order to balance the player count, then it will be automatically 
created in the player profile as well (without a country or sport club attached).
An e-mail is sent to every player with a linked user profile to notify them of their registration into a tournament.
"""


@tournmanet_router.post("/insert-players", status_code=201)
def insert_players(players: Tournament_registration, x_token=Header()):
    player_name_email = player_services.get_email_by_player_name(players.player_name)
    try:
        user = get_user_or_raise_401(x_token)
        check_player_name = tournament_services.check_player_name(
            players.player_name, players.tournament_id
        )
        if user.is_admin() or user.is_director():
            if not player_services.get_player_information(players.player_name):
                return player_services.create_player_info(
                    players
                ), tournament_services.insert_players_in_tournament(players)

            elif not check_player_name:
                if player_name_email != []:
                    emails.tournament_register_send_email(
                        player_name_email[0][0], players.player_name
                    )
                return tournament_services.insert_players_in_tournament(players)

            else:
                return Unauthorized("Player already registered to this tournament!")
        else:
            return Unauthorized(
                "Only Directors and Admin can insert players into tournament!"
            )
    except:
        return Unauthorized("You admin session has expired! Please login again.")
