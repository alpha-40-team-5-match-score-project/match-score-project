from fastapi import APIRouter, Header
from server.data.models import LoginData, Users
from server.services import user_services, player_services
from server.common import auth
from server.common import emails
from server.common.response import Unauthorized, BadRequest
from server.common.auth import get_user_or_raise_401
from mariadb import IntegrityError

users_router = APIRouter(prefix="/users")

"""
Endpoint is used to login a user and create a jwt token with an expiration time, specified in the auth.py global variables
"""


@users_router.post("/login")
def login(data: LoginData):
    user = user_services.try_login(data.email, data.password)

    if user:
        token = auth.create_token(user)

        return "Welcome back! Here's your login information.", {"token": token}
    else:
        return BadRequest("Invalid login data")


"""
Endpoint is used to register a new user into the system
"""


@users_router.post("/register")
def register(data: LoginData):
    user = user_services.create(data.email, data.password)
    emails.register_email(user.email)
    return user or BadRequest(f"Username {data.email} is taken.")


"""
Endpoint is used to request a promotion from a guest user to a director role
"""


@users_router.put("/request-director")
def request_director_promotion(x_token=Header()):
    member = get_user_or_raise_401(x_token)

    if not member.is_admin() and not member.is_director():
        user_services.request_promotion_to_director(member.user_id)
        return "Your application has been forwarded! Pending acception/refusal."
    else:
        return Unauthorized(
            "Only admin/directors can approve/refuse or your request has already been processed."
        )


"""
Endpoint is used to request a promotion from a guest user to a player role
"""


@users_router.put("/request-player")
def request_player_promotion(user: Users, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    existing_user = user_services.find_by_id(member.user_id)

    if not member.is_admin() and not member.is_director() and not member.is_player():
        user_services.request_promotion_to_player(existing_user, user)
        return "Your application has been forwarded! Pending acception/refusal."
    else:
        return "Only admin/directors can approve/refuse or your request has already been processed."


"""
Endpoint is used to promote a guest user to a director.
Can only be done by admin-level users.
"""


@users_router.put("/promote-director/{user_id}")
def promote_to_director(user_id: int, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    promoted_user = user_services.find_by_id(user_id)
    if member.is_admin() and promoted_user is not None:
        emails.promote_to_director_email_send(promoted_user.email)
        user_services.promote_to_director(user_id)
        return f"User has been promoted to Director!"
    else:
        return Unauthorized("Only an admin can promote guests or user does not exist!")


"""
Endpoint is used to promote a guest user to a player.
Can only be done by admin-level users.
"""


@users_router.put("/promote-player/{user_id}")
def promote_to_player(user_id: int, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    player_promote = user_services.find_by_id(user_id)
    if member.is_admin() and player_promote is not None:
        emails.promote_to_player(player_promote.email)
        user_services.promote_to_player(user_id)
        return f"User has been promoted to Player!"
    else:
        return Unauthorized("Only an admin can promote guests or user does not exist!")


"""
Endpoint is used to refuse a promotion of a guest user.
Can only be done by admin-level users.
"""


@users_router.put("/refuse/{user_id}")
def refuse_user(user_id: int, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    refused_member = user_services.find_by_id(user_id)
    if member.is_admin() and refused_member is not None:
        emails.refuse_promotion_email_send(refused_member.email)
        user_services.refuse_user_by_id(user_id)
        return f"User application has been refused!"
    else:
        return Unauthorized(
            "Only an admin can refuse applications or user does not exist!"
        )
