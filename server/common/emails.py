from mailjet_rest import Client
from datetime import date
import os
from typing import Any
from requests.models import Response

"""
Global variables used in the e-mail specification and adjustments
"""
api_key = "ee4b8501a6f6fb10d8684102d3ffd7c0"
api_secret = "32b50a60b81384bb2da6280c6048e96e"
SENDER_EMAIL = "vipercho_98@abv.bg"
RECIEVER_NAME = "Dear User"

_mailjet = Client(auth=(api_key, api_secret), version="v3.1")

"""
A function used for the creation of an e-mail and notify the user of his successful registration in the system
"""


def register_email(
    reciever_email: str, reciever_name=RECIEVER_NAME, **kwargs
) -> dict[str, list[dict[str, Any]]]:
    # print(request_type)
    data = {
        "Messages": [
            {
                "From": {"Email": "dark683@abv.bg", "Name": "Match Score Team"},
                "To": [{"Email": f"{reciever_email}", "Name": f"{reciever_name}"}],
                "TemplateID": 4346397,
                "TemplateLanguage": True,
                "Subject": "Match Score Team",
                "Variables": {},
            }
        ]
    }
    result = _mailjet.send.create(data)


"""
A function used for the creation of an e-mail and notify the user of his successful promotion to a director in the system
"""


def promote_to_director_email_send(
    reciever_email: str, reciever_name=RECIEVER_NAME
) -> dict[str, list[dict[str, Any]]]:
    data = {
        "Messages": [
            {
                "From": {"Email": "dark683@abv.bg", "Name": "Match Score Team"},
                "To": [{"Email": f"{reciever_email}", "Name": f"{reciever_name}"}],
                "TemplateID": 4346423,
                "TemplateLanguage": True,
                "Subject": "Request for director",
                "Variables": {},
            }
        ]
    }
    result = _mailjet.send.create(data)


"""
A function used for the creation of an e-mail and notify the user of his refusal for a promotion
"""


def refuse_promotion_email_send(
    reciever_email: str, reciever_name=RECIEVER_NAME
) -> dict[str, list[dict[str, Any]]]:
    data = {
        "Messages": [
            {
                "From": {"Email": "dark683@abv.bg", "Name": "Match Score Team"},
                "To": [{"Email": f"{reciever_email}", "Name": f"{reciever_name}"}],
                "TemplateID": 4347324,
                "TemplateLanguage": True,
                "Subject": "Request for Director",
                "Variables": {},
            }
        ]
    }
    result = _mailjet.send.create(data)


"""
A function used for the creation of an e-mail and notify the user of his successful promotion to a player in the system
"""


def promote_to_player(
    reciever_email: str, reciever_name=RECIEVER_NAME
) -> dict[str, list[dict[str, Any]]]:
    data = {
        "Messages": [
            {
                "From": {"Email": "dark683@abv.bg", "Name": "Match Score Team"},
                "To": [{"Email": f"{reciever_email}", "Name": f"{reciever_name}"}],
                "TemplateID": 4347347,
                "TemplateLanguage": True,
                "Subject": "Player promotion",
                "Variables": {},
            }
        ]
    }
    result = _mailjet.send.create(data)


"""
A function used for the creation of an e-mail and notify the user of his successful registration for a tournament
"""


def tournament_register_send_email(
    reciever_email: str, reciever_name: str
) -> dict[str, list[dict[str, Any]]]:
    data = {
        "Messages": [
            {
                "From": {"Email": "dark683@abv.bg", "Name": "Match Score Team"},
                "To": [{"Email": f"{reciever_email}", "Name": f"{reciever_name}"}],
                "TemplateID": 4347476,
                "TemplateLanguage": True,
                "Subject": "Tournament registration",
                "Variables": {},
            }
        ]
    }
    result = _mailjet.send.create(data)


"""
A function used for the creation of an e-mail and notify the user of his successful registration for a match
"""


def register_match_send_email(
    reciever_email: str, reciever_name=RECIEVER_NAME
) -> dict[str, list[dict[str, Any]]]:
    data = {
        "Messages": [
            {
                "From": {"Email": "dark683@abv.bg", "Name": "Match Score Team"},
                "To": [{"Email": f"{reciever_email}", "Name": f"{reciever_name}"}],
                "TemplateID": 4347715,
                "TemplateLanguage": True,
                "Subject": "You have been registered into a new match!",
                "Variables": {},
            }
        ]
    }
    test = _mailjet.send.create(data)
