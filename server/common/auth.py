from fastapi import HTTPException
from server.data.database import read_query
from server.data.models import Users
from server.common.response import Unauthorized
from datetime import datetime, timedelta
from jose import jwt, JWTError
from server.services.user_services import find_by_email

"""
Global variables used for the creation of a jwt token with a manageable expiry time
"""
ACCESS_TOKEN_EXPIRE_MINUTES = 120
SECRET_KEY = "secret"
ALGORITHM = "HS256"

"""
A function used for the creation of the jwt token
"""


def create_token(user: Users):
    to_encode = {"email": user.email}

    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})

    token = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return token


"""
A function used for extracting the email from the encrypted token
"""


def decode_email_from_token(token):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=ALGORITHM)
        email: str = payload.get("email")
    except JWTError:
        raise HTTPException(
            status_code=Unauthorized, detail=f"This token may expired or never existed!"
        )

    return email


def from_token(token: str) -> Users | None:
    return find_by_email(decode_email_from_token(token))


"""
A function used to check if the user is logged in or not
"""


def is_authenticated(token: str) -> bool:
    email = decode_email_from_token(token)
    data = read_query(
        """ SELECT email, user_role from Users where email = ?""", (email,)
    )
    return bool(data)


"""
A function used for validation of a user through the jwt token
"""


def get_user_or_raise_401(token: str) -> Users:
    if not is_authenticated(token):
        raise HTTPException(status_code=401)

    return from_token(token)
