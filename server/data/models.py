from datetime import date
from pydantic import BaseModel, constr

"""
A regex used for the email validation used in the registering of a user
"""
Temail = constr(
    regex="([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+"
)


class Role:
    GUEST = "guest"
    PLAYER = "player"
    DIRECTOR = "director"
    ADMIN = "admin"


class TypesOfTournaments:
    LEAGUE = "league"
    KNOCKOUT = "knockout"


class Users(BaseModel):
    user_id: int | None
    email: str
    password: str
    user_role: str
    player_name: str | None

    def is_admin(self):
        return self.user_role == Role.ADMIN

    def is_director(self):
        return self.user_role == Role.DIRECTOR

    def is_player(self):
        return self.user_role == Role.PLAYER

    @classmethod
    def from_query_result(cls, user_id, email, password, user_role, player_name):
        return cls(
            user_id=user_id,
            email=email,
            password=password,
            user_role=user_role,
            player_name=player_name,
        )


class LoginData(BaseModel):
    email: Temail
    password: str


class Players(BaseModel):
    name: str | None
    country_code: str | None
    sport_club_id: int | None
    match_played: int | None
    match_won: int | None
    tournament_played: int | None
    tournament_won: int | None

    @classmethod
    def from_query_result(
        cls,
        name,
        country_code,
        sport_club_id,
        match_played,
        match_won,
        tournament_played,
        tournament_won,
    ):
        return cls(
            name=name,
            country_code=country_code,
            sport_club_id=sport_club_id,
            match_played=match_played,
            match_won=match_won,
            tournament_played=tournament_played,
            tournament_won=tournament_won,
        )


class Countries(BaseModel):
    code: str | None
    name: str


class SportClubs(BaseModel):
    id: int | None
    name: str
    creation_date: str


class TournamentType(BaseModel):
    id: int | None
    type: str

    def is_league(self):
        return self.type == TypesOfTournaments.LEAGUE

    def is_knockout(self):
        return self.type == TypesOfTournaments.KNOCKOUT


class Tournaments(BaseModel):
    id: int | None
    title: str
    location: str
    start_date: date
    end_date: date
    tournament_types_id: int | str
    prize: int

    @classmethod
    def from_query_result(
        cls, id, title, location, start_date, end_date, tournament_types_id, prize
    ):
        return cls(
            id=id,
            title=title,
            location=location,
            start_date=start_date,
            end_date=end_date,
            tournament_types_id=tournament_types_id,
            prize=prize,
        )


class Tournament_registration(BaseModel):
    id: int | None
    tournament_id: int
    player_name: str
    player_total_score: int | None

    @classmethod
    def from_query_result(cls, id, tournament_id, player_name, player_total_score):
        return cls(
            id=id,
            tournament_id=tournament_id,
            player_name=player_name,
            player_total_score=player_total_score,
        )


class Matches(BaseModel):
    match_id: int | None
    round: int
    date: date
    tournament_id: int
    score_one: int
    score_two: int
    player_one: str
    player_two: str
    winner: str | None

    @classmethod
    def from_query_result(
        cls,
        match_id,
        round,
        date,
        tournament_id,
        score_one,
        score_two,
        player_one,
        player_two,
        winner,
    ):
        return cls(
            match_id=match_id,
            round=round,
            date=date,
            tournament_id=tournament_id,
            score_one=score_one,
            score_two=score_two,
            player_one=player_one,
            player_two=player_two,
            winner=winner,
        )
